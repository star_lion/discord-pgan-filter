import React, { Fragment, useState, useEffect } from 'react'
import * as R from 'ramda'
import * as dateFns from 'date-fns'

import { parseTime, validChannels, onChatChange } from './main'
import store from './store'
import Mon from './Mon'

let state = {
  ascend: true
}

const toggleMapStyle = () => store.toggle('mapStyleSatellite')

const toggleAscend = () => state.ascend = !state.ascend

export default function App(props) {
  const [descend, setDescend] = useState(true)

  let [ mapStyleSatellite ] = store.paths('mapStyleSatellite').link(useState())
  let [ channelTitle ] = store.paths('channelTitle').link(useState())
  let [ messages, unlink ] = store.paths('messages').link(useState())

  useEffect(() => unlink, [])

  let isValidChannel = validChannels.includes(channelTitle)
  //console.log(321, 'App render', messages)
  const nowTime = Date.now()
  let filteredMons = R.compose(
    R.reverse,
    R.sortBy(R.path(
      //['info', 'rank']
      ['meta', 'despawnTime']
    )),
    R.filter(R.pathEq(['meta', 'location'], 'West R1 Stafford')),
    //R.filter((message) => message.meta.despawnTime > nowTime)
    R.filter(R.pathSatisfies(R.lt(nowTime), ['meta', 'despawnTime'])) // spawned
  )(messages)

  return (
    <div>
      <div className='extension-controls'>

        <div>
          { filteredMons.length + ' Pokemon: ' }
          <button>All</button>
          <button>Spawned Only</button>
        </div>

        <div>
          <input type='text' placeholder='Location'  />
          <br />
          <input type='text' placeholder='Pokemon Name'  />
        </div>

        <div>
          <span>
            <select>
              <option value='despawnTime'>Despawn Time</option>
              <option value='rank'>Rank</option>
              <option value='messageTime'>Alert Time</option>
            </select>
          </span>
          <button onClick={ () => setDescend(!descend) }> { descend === true ? 'Descending' : 'Ascending' } </button>
        </div>

        <div>
          <button onClick={ toggleMapStyle }>Map: { mapStyleSatellite === true ? 'Satellite' : 'Streets' }</button>
          <button onClick={ onChatChange }>Refresh</button>
        </div>


      </div>

      <div className={`mon-grid ${mapStyleSatellite === true ? 'satellite-map' : '' } flex flex-row`}>
        { filteredMons.map((mon) => (
          <Mon mon={mon} key={mon.id} />
        )) }
      </div>
    </div>
  )
}

