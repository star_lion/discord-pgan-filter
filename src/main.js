console.log('Discord PGAN Filter loaded')

import React from 'react'
import { render } from 'react-dom'
import * as R from 'ramda'
import * as dateFns from 'date-fns'

import store from './store'
import App from './App'

const { mapObjIndexed } = R

const utils = {
  debounce: function(func, timeout = 300) {
    let timer;
    return (...args) => {
      if (!timer) {
        func.apply(this, args);
      }
      clearTimeout(timer);
      timer = setTimeout(() => {
        timer = undefined;
      }, timeout);
    }
  }
}

const queries = mapObjIndexed((selector) => () => document.body.querySelector(selector), {
  contentContainer: 'div[class^="content-"]',
  chatContainer: 'div[class^="chat-"]',
  messageList: 'div[class^="chat-"] main[class^="chatContent-"] div[class^="scrollerInner-"]',
  channelTitle: 'section[class^="title-"] div[class^="children-"]'
})

const selectors = {
  messages: 'div[class^="chat-"] main[class^="chatContent-"] div[class^="scrollerInner-"] div[class*="message-"]',
  timestamp: 'time',
  messageContent: 'div[class^="container-"]',
  messageTitle: 'div[class^="embedTitle-"]',
  messageDescription: 'div[class^="embedDescription-"]',
  messageFields: 'div[class^="embedFields-"]',
  gifImg: 'a[class*="embedThumbnail-"] img',
  mapImg: 'a[class*="embedMedia-"] img',
  pganLink: 'a[title="PGAN Map"]',
}

const state = {
  chatObserver: undefined
}

export const parseTime = (time) => dateFns.parse(time, 'p', new Date())

const timePattern = /Today at (\d+:\d+\ [P|A]M)/
//const timePattern = /Yesterday at (\d+:\d+\ [P|A]M)/

function isTimeWithinPastHour(messageElement, oneHourAgo) {
  const timeToday = messageElement.textContent.match(timePattern)
  if (timeToday === null) return false
  const messageDate = parseTime(timeToday[1])
  const isAfterOneHourAgo = dateFns.isAfter(messageDate, oneHourAgo)
  return isAfterOneHourAgo
}

const isMessageTimeAfterTime = R.curry((time, messageElement) => {
  const messageTime = dateFns.parseISO(messageElement.querySelector(selectors.timestamp).dateTime)
  return dateFns.isAfter(messageTime, time)
})

const isMessageTimeAfterTime2 = R.curry((time, message) => {
  let messageTimeDate = message.children[0].children.length === 3
    ? message.children[0].children[1].children[1].children[0].dateTime
    : message.children[0].children[0].children[0].dateTime

  messageTimeDate = dateFns.parseISO(messageTimeDate)

  return dateFns.getTime(messageTimeDate) > time
})

const isMonDespawnTimeAfterTime = R.curry((time, mon) => {
  return mon.meta.despawnTime > time
})

const monExample = {
  signature: undefined,
  meta: {
    messageTime: undefined,
    messageTimeString: undefined,
    despawnTime: undefined,
    despawnTimeString: undefined,
    location: undefined,
    messageElementId: undefined,
    gifSrc: undefined,
    mapSrc: undefined,
    pganLink: undefined,
  },
  info: {
    name: undefined,
    ivPercent: undefined,
    league: undefined,
    rank: undefined,
    pvpPercent: undefined,
    level: undefined,
    cp: undefined
  }
}

const ivTitlePattern = /\[(.*)\]([^-]*)-([^-]*)-([^-]*)/
const rareTitlePattern = /\[(\w*)\] (\w*)/
const cpPattern = /(\d*)CP/
const greatLeaguePattern = /^GL/
const ultraLeaguePattern = /^UL/
const rankPattern = /Rank\ (\d*)/
const despawnTimePattern = /Despawns \w*: (\d*:\d* [A|P]M)/

const titlePatterns = {
  location: /^\[(.*)\]/,
  name: /\[.*\]\ ([^-]*)\ -.*/,
  league: /-\ ([G|U])L/,
  rank: /Rank\ (\d*)\ /,
  ivPercent: /-\ (\d*)%/
}

const descriptionPatterns = {
  level: /L(\d*)\ /,
  cp: /-\ (\d*)CP\ -/,
  despawnTimeString: /Despawns \w*: (\d*:\d* \wM)/
}

const fieldsPatterns = {
  pvpPercent: /Rank:\ \d*\ \((.*)%\)/
}

const mapPatternsObjectToMatches = R.curry((patternsObject, string) => R.map(R.compose(
  R.prop(1),
  (pattern) => R.match(pattern, string)
), patternsObject))

const processTitleData = mapPatternsObjectToMatches(titlePatterns)

const processDescriptionData = mapPatternsObjectToMatches(descriptionPatterns)

const processFieldsData = mapPatternsObjectToMatches(fieldsPatterns)

function digitizeMessage3(message) {
  let messageTimeDate = message.children[0].children.length === 3
    ? message.children[0].children[1].children[1].children[0].dateTime
    : message.children[0].children[0].children[0].dateTime

  messageTimeDate = dateFns.parseISO(messageTimeDate)

  const messageTime = dateFns.getTime(messageTimeDate)
  const messageTimeString = dateFns.format(messageTimeDate, 'p')

  const titleText = message.children[1].children[0].children[0].children[0].textContent
  const descriptionText = message.children[1].children[0].children[0].children[1].textContent
  const fieldsText = message.children[1].children[0].children[0].children[2].textContent

  const mapImg = message.children[1].children[0].children[0].children.length === 6
    ? message.children[1].children[0].children[0].children[3].children[0]
    : undefined

  const gifImg = message.children[1].children[0].children[0].children.length === 6
    ? message.children[1].children[0].children[0].children[4].children[1]
    : message.children[1].children[0].children[0].children[3].children[1]
    //: undefined

  const pganLink = message.children[1].children[0].children[0].children[2].lastChild.lastChild.lastChild

  const titleData = processTitleData(titleText)
  const descriptionData = processDescriptionData(descriptionText)
  const fieldsData = processFieldsData(fieldsText)

  const despawnTime = dateFns.getTime(dateFns.parse(descriptionData.despawnTimeString, 'p', new Date()))
  const monId = `${titleData.name}-${titleData.location}-${despawnTime}${descriptionData.cp !== undefined ? ('-' + descriptionData.cp) : ''}`.toLowerCase().replace(/\s/g, '-')

  // build data
  const mon = {
    id: monId,
    meta: {
      messageTime,
      messageTimeString,
      despawnTime,
      despawnTimeString: descriptionData.despawnTimeString,
      location: titleData.location,
      messageElementId: message.id,
      gifSrc: gifImg !== undefined ? gifImg.src : undefined,
      mapSrc: mapImg !== undefined ? mapImg.src : undefined,
      pganLink: pganLink !== undefined ? pganLink.href : undefined
    },
    info: {
      name: titleData.name,
      league: titleData.league,
      rank: parseFloat(titleData.rank) || undefined ,
      ivPercent: parseFloat(titleData.ivPercent) || undefined,
      level: parseFloat(descriptionData.level) || undefined,
      cp: parseFloat(descriptionData.cp) || undefined,
      pvpPercent: parseFloat(fieldsData.pvpPercent) || undefined
    }
  }

  return mon
}

function digitizeMessage(message) {
  // text data processing
  const title = message.querySelector(selectors.messageTitle).textContent
  const description = message.querySelector(selectors.messageDescription).textContent
  const fields = message.querySelector(selectors.messageFields).textContent

  const titleData = processTitleData(title)
  const descriptionData = processDescriptionData(description)
  const fieldsData = processFieldsData(fields)

  const messageDate = dateFns.parseISO(message.querySelector(selectors.timestamp).dateTime)
  const messageTime = dateFns.getTime(messageDate)
  const messageTimeString = dateFns.format(messageDate, 'p')

  const despawnTime = dateFns.getTime(dateFns.parse(descriptionData.despawnTimeString, 'p', new Date()))
  const monId = `${titleData.name}-${titleData.location}-${despawnTime}${descriptionData.cp !== undefined ? ('-' + descriptionData.cp) : ''}`.toLowerCase().replace(/\s/g, '-')

  // element querying
  const gifImg = message.querySelector(selectors.gifImg)
  const mapImg = message.querySelector(selectors.mapImg)
  const pganLink = message.querySelector(selectors.pganLink)

  // build data
  const mon = {
    id: monId,
    meta: {
      messageTime,
      messageTimeString,
      despawnTime,
      despawnTimeString: descriptionData.despawnTimeString,
      location: titleData.location,
      messageElementId: message.id,
      gifSrc: gifImg !== null ? gifImg.src : undefined,
      mapSrc: mapImg !== null ? mapImg.src : undefined,
      pganLink: pganLink !== null ? pganLink.href : undefined
    },
    info: {
      name: titleData.name,
      league: titleData.league,
      rank: parseFloat(titleData.rank) || undefined ,
      ivPercent: parseFloat(titleData.ivPercent) || undefined,
      level: parseFloat(descriptionData.level) || undefined,
      cp: parseFloat(descriptionData.cp) || undefined,
      pvpPercent: parseFloat(fieldsData.pvpPercent) || undefined
    }
  }

  return mon
}

export const validChannels = ['stafford-pvp', 'stafford-hundo', 'stafford-rares']

export function onChatChange() {
  console.log(321, 'onChatChange')
  const channelTitle = queries.channelTitle().textContent

  if (channelTitle !== null && validChannels.includes(channelTitle)) {
    //const messages = Array.from(queries.messageList().querySelectorAll('div[class^="message-"]'))
    const messages = document.body.querySelectorAll(selectors.messages)

    const oneHourAgo = dateFns.subHours(Date.now(), 1)
    const twoHoursAgo = dateFns.subHours(Date.now(), 1)
    const fourHoursAgo = dateFns.subHours(Date.now(), 4)
    //const twelveHoursAgo = dateFns.subHours(Date.now(), 12)
    //const thirtySixHoursAgo = dateFns.subHours(Date.now(), 36)
    const timeThreshold = fourHoursAgo

    const titlePattern = (channelTitle === 'stafford-rares') ? rareTitlePattern : ivTitlePattern

    const mons = R.compose(
      R.uniqBy(R.prop('id')),
      R.map(digitizeMessage3),
      R.filter(isMessageTimeAfterTime2(dateFns.getTime(timeThreshold))),
    )(messages)

    store.set('messages', mons)
  } else {
    store.set('messages', [])
  }
  store.set('channelTitle', channelTitle)
}

function main() {
  const observer = new MutationObserver(utils.debounce(onChatChange, 30000));
  //const observer = new MutationObserver(utils.debounce(onChatChange, 1000));
  observer.observe(queries.contentContainer(), { childList: true, subtree: true });
  onChatChange()

  const root = document.createElement('div')
  root.id = 'pgan-filter-extension'
  root.className = 'pgan-filter-extension'
  document.body.prepend(root)
  render(<App />, root)
}

window.addEventListener('load', () => {
  //if (config.meta.development == true && window.location.href.indexOf('?off') !== -1) return
  if (window.location.href.indexOf('?off') !== -1) return
  setTimeout(main, 2000)
})

window.appDebug = { queries, timePattern }
