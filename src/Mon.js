import React, { Fragment, useState, useEffect } from 'react'

const monExample = {
  signature: '',
  meta: {
    messageTime: undefined,
    despawnTime: undefined,
    location: undefined,
    messageElementId: undefined,
    gifSrc: undefined,
    mapSrc: undefined,
    mapLink: undefined,
  },
  info: {
    name: '',
    ivPercent: undefined,
    league: undefined,
    rank: undefined,
    rankPercent: undefined,
  }
}

export default function Mon(props) {
  const { name, league, rank, pvpPercent, ivPercent } = props.mon.info
  const { despawnTimeString, mapSrc, pganLink, location } = props.mon.meta

  return (
    <div className={`mon-container ${ league !== undefined ? league : '' }`}>
      <div className="mon-header">
        <div> { name } { despawnTimeString } </div>
        { (league !== undefined && rank !== undefined)
          ? (<div> { `${league} #${rank} (${pvpPercent}%)` } </div>)
          : (<div> { ivPercent || '??' }% </div>)
        }
      </div>
      <div className='absolute mon-location'> { location } </div>
      <div className='mon-map' style={{ width: '198px', height: '300px', overflow: 'hidden' }}>
        <img src={ mapSrc } style={{ height: '100%' }} />
      </div>
      <a className='pgan-link' href={ pganLink } target='_blank'></a>
    </div>
  )
}
