import ClearX from 'clearx'

let store = new ClearX({
  messages: [],
  channelTitle: '',
  mapStyleSatellite: false
})

export default store
