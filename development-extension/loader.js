const PORT = '9901'

const script = document.createElement('script')
script.src = `http://localhost:${PORT}/main.js?t=${Date.now()}`
document.head.appendChild(script)

const link = document.createElement('link')
link.href = `http://localhost:${PORT}/style.css?t=${Date.now()}`
link.rel = 'stylesheet'
link.type = 'text/css'
document.head.appendChild(link)
